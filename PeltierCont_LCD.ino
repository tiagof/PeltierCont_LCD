/*

 Compatible with Hitachi HD44780 driver

*/

// include the library code:
#include <LiquidCrystal.h>
#include <PID_v1.h>


// These constants won't change. They're used to give names to the pins used:
// with the arduino pin number it is connected to
//const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
const int rs = 7, en = 6, d4 = 5, d5 = 4, d6 = 3, d7 = 2; //LCD
const int Peltier_cold_pin = A0;  // Analog input pin that the thermistor is attached to Perltier Cold side
const int Peltier_hot_pin = A1;  // Analog input pin that the thermistor is attached to Perltier Cold side
const int Cold_setpoint_pin = A2; // Analog input pin that the set point potentiometer is attached to
const int PWM_peltier_out_pin = 9; //Pin 9 is PB1
const int OverTemp_out_pin = 8; //Pin 8 is PB0 RED LED (Overtemeperature)


#define HOT_MAX_VALUE 25
#define HOT_MIN_VALUE 0

#define MAX_PWM 240 //limits the maximum power
#define MIN_PWM 0 //limits the minimum power

#define TCOLD_MIN -18
#define TCOLD_MAX 0

// Resistor for voltage divider
#define R1 10000

#define SAMPLESIZE 20  //number of samples for temperature averaging

//Degree character
byte degree[8] = {
  B00110,
  B01001,
  B01001,
  B00110,
  B00000,
  B00000,
  B00000,
};

// Steinhart-Hart and Hart Coefficients for -20° to 0°
//https://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
float a1 = 1.142837781e-03, b1 = 2.323139415e-04, c1 = 0.9224900344e-07;

// Steinhart-Hart and Hart Coefficients 5°C to 45°C
//https://www.thinksrs.com/downloads/programs/Therm%20Calc/NTCCalibrator/NTCcalculator.htm
float a2 = 1.125614740e-03, b2 = 2.346500768e-04, c2 = 0.8600178326e-07;

double PWMout;
double TCold, THot;
double Tsetpoint = 0;

//Specify the links and initial tuning parameters
//Ziegler-Nichols Method
//#define Ku 300  //Gain that provoques constant oscilations
//#define Tu 10  //10s time constant between oscillations
//double Kp=0.2*Ku;
//double Ki=Kp/(Tu/2);
//double Kd=Kp*(Tu/3);

/*Ku=400
 * Kp=0.2*Ku
 * Kp=0.6*Ku
 * 
 * Tu=20s
 * Ti=Tu/2
 * Ki=Kp/Ti
 * 
 * Td=Tu/3
 * Kd=Kp*Td
*/
//adjusted tuning parameters
double Kp=60;
double Ki=10;
double Kd=12;

//Specify the links and initial tuning parameters
//double Kp=50;
//double Ki=10;
//double Kd=1;

//PID myPID(&TCold, &PWMout, &Tsetpoint, Kp, Ki, Kd, DIRECT);  /Output increase makes HEAT
PID myPID(&TCold, &PWMout, &Tsetpoint, Kp, Ki, Kd, P_ON_M, REVERSE); //Output increase makes COOLING //P_ON_M specifies that Proportional on Measurement be used
//PID myPID(&TCold, &PWMout, &Tsetpoint, Kp, Ki, Kd, REVERSE); 

// initialize the library by associating any needed LCD interface pin
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

void setup() {
  
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  //Create Degree Symbol
  lcd.createChar(0, degree);
  //lcd.createChar(1, degreeC);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  //lcd.print("hello, world!");
  //lcd.print(F("TCold, Temp Hot"));
  //analogReadResolution(10);
  lcd.setCursor(0, 0); // set the cursor to column 0, line 0
  lcd.print(F("Cryoscopy Cooler"));
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  lcd.print(F("ETH  Tiago N. v2"));
  pinMode(OverTemp_out_pin, OUTPUT);
  analogReference(EXTERNAL);   //The voltage referenece for the ADC is external power 3.3V
  Serial.println(F("CC"));
  Serial.println(F("Cryoscopy Cooler"));
  Serial.println(F("Tiago N. V2"));
  Serial.print("PID Gains= Kp:");
  Serial.print(Kp);
  Serial.print(" Ki:");
  Serial.print(Ki);
  Serial.print(" Kd:");
  Serial.println(Kd);
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(MIN_PWM, MAX_PWM);
  myPID.SetSampleTime(200);
  delay(4000);
}

void loop() {

  Read_TEMP (); //Read temperatures
  Tsetpoint = Read_Setpoint (); //Read setpoint potentiomenter
  myPID.Compute();  //compute PID

  Serial.print("Setpoint=");
  Serial.print(Tsetpoint);
  Serial.print("°C  ");

  Serial.print("INPUT=");
  Serial.print(TCold);
  Serial.print("°C  ");

  Serial.print("ERROR=");
  Serial.print(Tsetpoint-TCold);
  Serial.print("°C  ");

  Serial.print("PWM_OUT1=");
  Serial.println(PWMout);
  
  if (THot > HOT_MAX_VALUE || THot < HOT_MIN_VALUE )
  {
    digitalWrite(OverTemp_out_pin, HIGH); //Turn on RED LED
    analogWrite(PWM_peltier_out_pin, 0);  //Disable PWM
    lcd.setCursor(0, 0); // set the cursor to column 0, line 0
    lcd.print(F("NO COOLING WATER"));    
    }
    else if (TCold > 40 || TCold < -25)
      {
      digitalWrite(OverTemp_out_pin, HIGH); //Turn on RED LED
      analogWrite(PWM_peltier_out_pin, 0);  //Disable PWM
      lcd.setCursor(0, 0); // set the cursor to column 0, line 0
      lcd.print(F("INPUT COLD ERROR"));
      }
      else{
        digitalWrite(OverTemp_out_pin, LOW); //Turn off RED LED
        analogWrite(PWM_peltier_out_pin, PWMout); //Update PWM
        lcd.setCursor(0, 0); // set the cursor to column 0, line 0
        lcd.print(F("Setpoint: "));
        lcd.print(Tsetpoint,0);
        lcd.write(byte(0));   //print Degree character
        lcd.print("C   ");
        }

  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  lcd.print("C:");
  lcd.print(TCold,1);  //print with 1 decimal case
  lcd.write(byte(0));   //print Degree character
  //lcd.print("C,");
  lcd.print(" H:");
  lcd.print(THot,1);  //print with 1 decimal case
  lcd.write(byte(0)); //print Degree character
  lcd.print("  ");

  //lcd.clear();
  delay(200);
  
}

void Read_TEMP (void)
{
  int i=0;
  float Vo = 0;
  float logR2, R2;
  for ( i = 0; i < SAMPLESIZE ; i ++)  //measure values for averaging
    {
      // measure the NTC Cold side value:
      Vo = Vo + analogRead(Peltier_cold_pin);
      delay(2);
      }
  Vo = Vo / SAMPLESIZE;  //average
  R2 = R1 * (1023.0 / Vo - 1.0);
  logR2 = log(R2);
  TCold = (1.0 / (a1 + b1*logR2 + c1*logR2*logR2*logR2));
  TCold = TCold - 273.15;  //°C
  //T = (T * 9.0)/ 5.0 + 32.0; //°F

  Vo = 0;
  i=0;
  for ( i = 0; i < SAMPLESIZE ; i ++) //measure values for averaging
  {
    // measure the NTC Hot side value:
    Vo = Vo + analogRead(Peltier_hot_pin);
    delay(2);
  }
  Vo = Vo / SAMPLESIZE;    
  R2 = R1 * (1023.0 / Vo - 1.0);
  logR2 = log(R2);
  THot = (1.0 / (a2 + b2*logR2 + c2*logR2*logR2*logR2));
  THot = THot - 273.15;  //°C
  //T = (T * 9.0)/ 5.0 + 32.0; //°F

  // print the results to the Serial Monitor:
  Serial.print("sensor_cold = ");
  Serial.print(TCold);
  Serial.println(" °C");
  Serial.print("sensor_hot = ");
  Serial.print(THot);
  Serial.println(" °C");
  //Serial.print("\t output = ");
  //Serial.println(outputValue);
}

int Read_Setpoint (void)
{
  int Vo = 0;
  int i = 0;
  int Setpoint ;
    
  for ( i = 0; i < 10 ; i ++)
    {
      Vo = Vo + analogRead(Cold_setpoint_pin);
      delay(2);
    }
  //Serial.print("VoltinputVo_sum=");
  //Serial.println(Vo);
  Vo = round((float)Vo / 10);
    
   // map it to the range of -20 -10:
  Setpoint = map(Vo, 0, 1023, TCOLD_MIN, TCOLD_MAX);
  //Setpoint = map(Vo, 0, 1023, 0, 255);
  //Serial.print("VoltinputVo_average=");
  //Serial.print(Vo);
  return Setpoint;
}
