## Introduction
PeltierCont_LCD is an Arduino Software used to control a Peltier Element.
It also controls an 16*2 LCD displaying the Hot and Cold Temperatures of the Peltier.

The Data is obtained using an ADC that connects to the computer with USB.
The ADC Hardware communicates with serial port commands, and the project is available at:
https://gitlab.ethz.ch/tiagof/PeltierCont_LCD

### Software Dependencies
This application uses the libraries:
- LiquidCrystal
- PID_v1

### Hardware Dependencies
Self made PCB wit the main components:
- ATMEL-ATMEGA328P
- Peltier (8 Ampére)
- 2x NTC Thermistors
- 1 LCD
- Power MOSFET (Peltier Driver)

### Installing The libraries
Change the Project location in the arduino application:
- "File"->"Preferences"->"Sketchbook location:"
- Should look like: /home/user/PeltierCont_LCD


## Hadware Connections
- GND, TX, RX, RESET
![Cryoscopy Controller](https://gitlab.ethz.ch/tiagof/PeltierCont_LCD/-/raw/master/controller.jpg)

## Upload the Sketch
![Cryoscopy Controller](https://gitlab.ethz.ch/tiagof/PeltierCont_LCD/-/raw/master/upload.jpg)

## Cryoscopy Setup
![Cryoscopy Setup](https://gitlab.ethz.ch/tiagof/PeltierCont_LCD/-/raw/master/setup.jpg)

## Schematic
![Cryoscopy Setup](https://gitlab.ethz.ch/tiagof/PeltierCont_LCD/-/raw/master/schematic.jpg)
